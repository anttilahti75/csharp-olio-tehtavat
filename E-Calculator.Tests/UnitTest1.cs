using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace E_Calculator.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void MultiplyTest()
        {
            Calculator calc = new Calculator();

            int number1 = 3;
            int number2 = 5;
            int expected = 15;
            int actual = calc.Multiply(number1, number2);

            Assert.AreEqual(expected, actual);
        }
    }
}
