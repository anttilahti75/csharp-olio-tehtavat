using System;

namespace EDelegaatit
{
    public class OP{
        public static double LaskeKorko(
            double lainaSumma,
            double aika,
            double korkoprosentti)
        {
            // tasalyhennys
            return 0.5 * lainaSumma * aika * korkoprosentti / 100;
        }
    }

    public class Kaveri
    {
        public static double MaksaPariTonnia(
            double summa,
            double aika,
            double prosentti
            )
        {
            return aika * 2000;
        }

    }
    class Program
    {
        // määritellään delegaatti
        delegate double Korkolaskuri(double p, double t, double r);
        // delegaatti-muuttuja ja kiinnitetään siihen OP:n metodi
        static Korkolaskuri korkolaskuri = OP.LaskeKorko;
        static void Main(string[] args)
        {
            // lainan tiedot
            double laina = 100000;
            double aika = 10;
            double korko = 1.75;

            // kutsutaan delegaatti-muuttujassa olevaa metodia
            double kokokorko = korkolaskuri.Invoke(laina, aika, korko);

            Console.WriteLine($"Kokonaissumma OP:ssa on {kokokorko} euroa lainalla {laina} eurolle");

            // kiinnitetään delegaattiin uusi toiminto
            korkolaskuri = Kaveri.MaksaPariTonnia;
            kokokorko = korkolaskuri.Invoke(laina, aika, korko);
            Console.WriteLine($"Kaveri haluaa {kokokorko} euroa {laina} eurolle.");
        }
    }
}