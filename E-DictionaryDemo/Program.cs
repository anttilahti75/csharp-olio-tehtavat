using System;
using System.Collections.Generic;
namespace EDictionaryDemo
{
    class Person
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string ID { get; set; }

        public override string ToString()
        {
            return $"{Firstname} {Lastname} ID = {ID}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, Person> persons = new Dictionary<string, Person>();

            Person p1 = new Person() { Firstname = "Kirsi", Lastname = "Kernel", ID = "1"};
            Person p2 = new Person() { Firstname = "Netta", Lastname = "Niilonen", ID = "2"};
            Person p3 = new Person() { Firstname = "Matti", Lastname = "Mainio", ID = "3"};

            persons.Add(p1.ID, p1);
            persons.Add(p2.ID, p2);
            persons.Add(p3.ID, p3);

            Console.WriteLine($"Kokoelmassa on {persons.Count} henkilöä.");
            Console.WriteLine("Henkilöiden ID:t ovat:");
            foreach(string id in persons.Keys)
            {
                Console.WriteLine(id);
            }
            Console.WriteLine("Henkilöiden tiedot ovat:");
            foreach(Person person in persons.Values)
            {
                Console.WriteLine(person); // ToString() ajetaan automaattisesti
            }

            string ID = "2";

            if(persons.ContainsKey(ID))
            {
                Console.WriteLine($"Löytyi henkilö {persons[ID]}");
            }
            else
            {
                Console.WriteLine($"Ei ole henkilöä, jonka ID on {ID}.");
            }
        }
    }
}