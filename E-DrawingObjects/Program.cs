using System;

namespace EDrawingObjects {
    abstract class DrawingObject {
        public string Name { get; set; }

        public void DoSomething() {
            Console.WriteLine("Do something!");
        }

        public abstract string Color { get; set; }

        public abstract void Draw();
    }

    class Circle : DrawingObject {
        public override string Color { get; set; }

        public override void Draw() {
            Console.WriteLine("Draw circle");
        }
    }

    class Square : DrawingObject {
        public override string Color { get; set; }

        public override void Draw() {
            Console.WriteLine("Draw square");
        }
    }

    class Program {
        static void Main(string[] args) {
            // DrawingObject do = new DrawingObject();
            Circle circle = new Circle();
            circle.Color = "Red";
            circle.Name = "Ympyrä";
            circle.DoSomething();
            circle.Draw();

            Square square = new Square();
            square.DoSomething();
            square.Draw();
        }
    }
}
