using System;

namespace E_Koostaminen
{
    class IDCard
    {
        public string Name { get; set; }
        
        ~IDCard()
        {
            Console.WriteLine($"IDCard {Name} is destroyed!");
        }
    }

    class Teacher
    {
        public void UseCard(IDCard card)
        {
            Console.WriteLine($"Teacher uses IDCard: {card.Name}");
        }

        ~Teacher()
        {
            Console.WriteLine("Teacher is destroyed!");
        }
    }

    class Computer
    {
        public string Name { get; set; }

        ~Computer()
        {
            Console.WriteLine($"Computer {Name} is destroyed!");
        }
    }

    class Classroom
    {
        private Computer computer; // reference, computer-object is created elsewhere

        public Classroom()
        {
        }

        public Classroom(Computer computer)
        {
            this.computer = computer; // "this" refers to computer in this class
        }

        public void UseComputer()
        {
            Console.WriteLine($"Classroom uses computer {computer.Name}");
        }

        ~Classroom()
        {
            Console.WriteLine("Classroom is destroyed!");
        }
    }

    class Department
    {
        public string Name { get; set; }

        ~Department()
        {
            Console.WriteLine($"Department {Name} is destroyed!");
        }
    }

    class JAMK
    {
        private Department department = new Department();

        public JAMK()
        {
            department.Name = "IT";
        }

        public override string ToString()
        {
            return $"JAMK department is {department.Name}";
        }

        ~JAMK()
        {
            Console.WriteLine($"JAMK is destroyed!");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            TestAssociation();
            //TestAggregation();
            //TestComposition();
        }

        static void TestAssociation()
        {
            IDCard card = new IDCard() { Name = "MasterCard" };
            Teacher teacher = new Teacher();
            teacher.UseCard(card);

            teacher = null;
            GC.Collect();

            System.Threading.Thread.Sleep(1000);

            Console.WriteLine($"Card name is {card.Name}.");

            Console.WriteLine("Program ends!");
        }

        static void TestAggregation()
        {
            Computer computer = new Computer() { Name = "Lenovo" };
            Classroom classroom = new Classroom(computer);
            classroom.UseComputer();

            classroom = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            System.Threading.Thread.Sleep(3000);
            
            Console.WriteLine($"Computer name is {computer.Name}");

            Console.WriteLine("Program ends!");
        }

        static void TestComposition()
        {
            JAMK jamk = new JAMK();

            Console.WriteLine(jamk);

            jamk = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            System.Threading.Thread.Sleep(1000);

            Console.WriteLine("Program ends!");
        }
    }
}