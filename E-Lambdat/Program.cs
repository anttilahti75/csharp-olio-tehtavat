using System;
using System.Collections.Generic;
using System.Linq;

namespace ELambdat
{
    class Friend
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Country { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Friend> friends = new List<Friend>()
            {
                new Friend { Name = "John Doe", Email = "john@usa.net", Country = "USA" },
                new Friend { Name = "Jussi Kuukka", Email = "jussi@gmail.com", Country = "FIN" },
                new Friend { Name = "Teppo Testaaja", Email = "teppo@teppo.fi", Country = "FIN" },
            };

            Console.WriteLine($"Kavereita on {friends.Count}");
            Console.WriteLine("Nimet: ");
            friends.ForEach(x => Console.WriteLine(x.Name));
             /*
             foreach (Friend f in friends)
             {
                 Console.WriteLine(f.Name);
             }
             */
             Console.WriteLine("Kaverit aakkosjärjestyksessä:");
             friends.OrderBy(x => x.Name).ToList().ForEach(x => Console.WriteLine(x.Name));

             Console.WriteLine("Suomalaiset:");
             var suomalaiset = friends.Where(x => x.Country == "FIN").ToList();
             suomalaiset.ForEach(x => Console.WriteLine(x.Name));
        }
    }
}