using System;

namespace ENotes {
    interface ICanMakeANote {
        string MakeNote(string textToSave);
    }

    class Tablet : ICanMakeANote {
        public string CPU { get; set; }
        public void ShowVideo(string url) {
            Console.WriteLine("Video is playing...");
        }
        public string MakeNote(string textToSave) {
            return $"Your note {textToSave} is successfully written to memory!";
        }
    }

    class Paper : ICanMakeANote {
        // fields, properties, constructors, methods...
        public string MakeNote(string textToSave) {
            return $"I have written your note {textToSave} to paper.";
        }
    }
    class Program {
        static void Main(string[] args) {
           Console.WriteLine("Annapa muistiinpano:");
           string note = Console.ReadLine();

           Tablet tabi = new Tablet();
            tabi.CPU = "AMD..";
            tabi.ShowVideo("url");
            Console.WriteLine(tabi.MakeNote(note));

            // paper...
        }
    }
}
