using System;
using System.Collections.Generic;
using System.IO;

namespace E_Pelaajat
{
    class Pelaaja
    {
        public string Etunimi { get; set; }
        public string Sukunimi { get; set; }
        public string Pelipaikka { get; set; }
        public string Numero { get; set; }

        public Pelaaja() { }

        public Pelaaja(string etunimi, string sukunimi, string pelipaikka, string numero)
        {
            Etunimi = etunimi;
            Sukunimi = sukunimi;
            Pelipaikka = pelipaikka;
            Numero = numero;
        }

        public override string ToString()
        {
            return $"{Etunimi} {Sukunimi} {Pelipaikka} #{Numero}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // luetaan pelaajat levyltä
                Console.WriteLine("Luetaan pelaajat tiedostosta.");
                string splitChar = ";";
                string filename = "Jyp.csv";
                // = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Jyp.csv";

                string[] lines = File.ReadAllLines(filename);
                List<Pelaaja> pelaajat = new List<Pelaaja>();
                foreach(string line in lines)
                {
                    string[] words = line.Split(splitChar.ToCharArray());
                    Pelaaja pelaaja = new Pelaaja(words[0], words[1], words[2], words[3]);
                    pelaajat.Add(pelaaja);
                    Console.WriteLine(pelaaja);
                }
                Console.WriteLine($"Joukkueessa on {pelaajat.Count} pelaajaa.");

            } catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}