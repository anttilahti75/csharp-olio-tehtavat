using System;
using E1Cars;

namespace E1Cars
{
    class Car
    {
        // properties
        public string Model { get; set; }
        public string Color { get; set; }
        public string Engine { get; set; }
        public int Speed { get; set; }
        public bool FuzzyDices { get; set; }

        // methods
        public bool Accelerate(int value)
        {
            Speed += value;
            return true;
        }

        public string Sound()
        {
            return "Vroom!";
        }

        public override string ToString()
        {
            return $"Car: {Model} {Color} {Engine}";
        }

        ~Car()
        {
            Console.WriteLine("Car object destroyed!");
        }
    }
}