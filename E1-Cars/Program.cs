using System;
using E1Cars;

namespace E1Cars
{
    class Program
    {
        static void Main(string[] args)
        {
            // create one instance
            Car car = new Car();
            car.Model = "Fiat";
            car.Speed = 45;
            car.Engine = "1.0";
            car.Color = "Black";
            car.FuzzyDices = true;

            if (car.Accelerate(100)) Console.WriteLine("Accelerated successfully!");
            else
            {
                // ...
            }

            Console.WriteLine(car.ToString());
        }
    }
}