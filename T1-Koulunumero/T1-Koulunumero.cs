using System;

namespace T1KoulunumeroApplication
{
   class Koulunumero
   {
      static void Main(string[] args)
      {
            
            Console.Write("Give point limit: ");

            string input = Console.ReadLine();
            int point = 0;

            bool success = Int32.TryParse(input, out point);

            if (success == true) 
            {
                switch (point) 
                {
                    case 0:
                    case 1: Console.WriteLine("School number is 0");
                        break;

                    case 2:
                    case 3: Console.WriteLine("School number is 1");
                        break;

                    case 4:
                    case 5: Console.WriteLine("School number is 2");
                        break;

                    case 6:
                    case 7: Console.WriteLine("School number is 3");
                        break;
                    
                    case 8:
                    case 9: Console.WriteLine("School number is 4");
                        break;

                    case 10:
                    case 11: Console.WriteLine("School number is 5");
                        break;
                }
            }
            else 
            {
                Console.WriteLine("You did not write a number.");
            }

            Console.ReadLine();
        }
    }
}