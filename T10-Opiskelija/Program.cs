using System;

namespace T10OpiskelijaApplication {
    class Program {
        // The main program
        static void Main(string[] args)
        {

            Random rnd = new Random();
            object[] studentsArray = new object[5];

            Student s1 = new Student
            {
                Id = rnd.Next(1000),
                Firstname = "Matti",
                Lastname = "Meikäläinen",
                Age = 20,
                Email = "s1@school"
            };
            
            Student s2 = new Student
            {
                Id = rnd.Next(1000),
                Firstname = "Frankie",
                Lastname = "Manning",
                Age = 20,
                Email = "s2@school"
            };

            Student s3 = new Student
            {
                Id = rnd.Next(1000),
                Firstname = "Maija",
                Lastname = "Meikäläinen",
                Age = 20,
                Email = "s3@school"
            };

            Student s4 = new Student
            {
                Id = rnd.Next(1000),
                Firstname = "Pelle",
                Lastname = "Hermanni",
                Age = 20,
                Email = "s4@school"
            };

            Student s5 = new Student
            {
                Id = rnd.Next(1000),
                Firstname = "Poca",
                Lastname = "Hontas",
                Age = 20,
                Email = "s5@school"
            };

            studentsArray[0] = s1;
            studentsArray[1] = s2;
            studentsArray[2] = s3;
            studentsArray[3] = s4;
            studentsArray[4] = s5;

            for (int i = 0; i < studentsArray.Length; i++)
            {
                Console.WriteLine(studentsArray[i]);
            }

            Console.ReadLine();
        }
    }
}