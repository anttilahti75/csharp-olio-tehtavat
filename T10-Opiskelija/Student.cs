using System;

namespace T10OpiskelijaApplication
{
    class Student
    {
        private int _id;
        private string _firstname;
        private string _lastname;
        private int _age;
        private string _email;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Firstname
        {
            get { return _firstname; }
            set { _firstname = value; }
        }

        public string Lastname
        {
            get { return _lastname; }
            set { _lastname = value; }
        }

        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public override string ToString()
        {
            return $"id: {_id}\t Full Name: {_firstname} {_lastname} age: {_age} email: {_email}";
        }
    }
}