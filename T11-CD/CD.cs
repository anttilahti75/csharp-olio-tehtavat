using System;
using System.Collections.Generic;

namespace T11CDApplication
{
    class CD
    {
        public string Artist { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public float Price { get; set; }
        private string[] _songs = new string[15];

        public string this[int i]
        {
            get { return _songs[i]; }
            set { _songs[i] = value; }
        }

        public int GetSongArrayLength()
        {
            return _songs.Length;
        }

        public int CanAdd()
        {
            int nulls = 0; 
            for (int i = 0; i < _songs.Length; i++)
            {
                if (this[i] == null)
                {
                    nulls += 1;
                }
            }
            return nulls;
        }

        public override string ToString()
        {
            return $"CD:\n-Artist: {Artist}\n-Name: {Name}\n-Genre: {Genre}\n-Price: {Price}€";
        }
    }
}