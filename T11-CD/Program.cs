using System;

namespace T11CDApplication
{
    class Program
    {
        // The main program
        static void Main(string[] args)
        {

            object[] cdArray = new object[2];

            CD cd1 = new CD
            {
                Artist = "Nightwish",
                Name = "Endless Forms Most Beautiful",
                Genre = "Symphonic metal",
                Price = 19.9f
            };
            

            CD cd2 = new CD
            {
                Artist = "Gomorrah",
                Name = "Gomorrah",
                Genre = "Technical Death Metal",
                Price = 7.99f
            };

            cdArray[0] = cd1;
            cdArray[1] = cd2;

            cd1[0] = "Shudder Before the Beautiful - 06:29";
            cd1[1] = "Weak Fantasy - 05:23";
            cd1[2] = "Elan - 04:45";
            cd1[3] = "Yours Is an Empty Hope - 05:34";
            cd1[4] = "Our Decades in the Sun - 06:37";
            cd1[5] = "My Walden - 04:38";
            cd1[6] = "Endless Forms Most Beautiful - 05:07";
            cd1[7] = "Edema Ruh - 05:15";
            cd1[8] = "Alpenglow - 04:45";
            cd1[9] = "The Eyes of Sharbat Gula - 06:03";
            cd1[10] = "The Greatest Show on Earth - 24:00  ";

            cd2[0] = "Ember - 03:51";
            cd2[1] = "From Earthen Ruins - 03:43";
            cd2[2] = "Predatory Reich - 02:25";
            cd2[3] = "Frailty - 02:39";
            cd2[4] = "For Those of Eld - 03:38";
            cd2[5] = "The Carnage Wrought - 03:45";
            cd2[6] = "The Blade Itself - 02:24";
            cd2[7] = "Of Ghosts and the Grave - 03:54";


            // CD 1
            Console.WriteLine("\n" + cd1);
            Console.WriteLine("Songs:");

            // Loop through cd and print each song
            for (int i = 0; i < cd1.GetSongArrayLength(); i++)
            {
                if (cd1[i] != null)
                {
                    Console.WriteLine("--- Name: " + cd1[i]);
                }
            }

            Console.WriteLine("Can add " + cd1.CanAdd() + " songs until CD is full");

            // CD 2
            Console.WriteLine("\n" + cd2);
            Console.WriteLine("Songs:");

            // Loop through cd and print each song
            for (int i = 0; i < cd2.GetSongArrayLength(); i++)
            {
                if (cd2[i] != null)
                {
                    Console.WriteLine("--- Name: " + cd2[i]);
                }
            }
            
            Console.WriteLine("Can add " + cd2.CanAdd() + " songs until CD is full");

            Console.ReadLine();
        }
    }
}