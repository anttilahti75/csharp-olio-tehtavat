using System;

namespace T11CDApplication
{
    class Song
    {
        public int Track { get; set; }
        public string Name { get; set; }
        public string Length { get; set; }

        public override string ToString()
        {
            return $"Track: {Track}\t Name: {Name} Length: {Length}";
        }
    }
}