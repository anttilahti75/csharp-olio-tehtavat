using System;

namespace T12TankApplication
{
    class Tank
    {
        public string Name { get; set; }
        public string Type { get; set; }
        private int _crewNumber = 4;
        private float _speed = 0;
        private float _speedMax = 100;

        public int CrewNumber
        {
            get { return _crewNumber; }
            set {
                if (value >= 2 && value <= 6) _crewNumber = value;
            }
        }

        public float SpeedMax
        {
            get { return _speedMax; }
        }

        public float Speed
        {
            get { return _speed; }
        }

        public bool AccelerateTo(float value)
        {
            if (value > _speed && value <= _speedMax)
            {
                _speed = value; return true; 
            }
            else
            {
                return false;
            }
        }

        public bool SlowTo(float value)
        {
            if (value >= 0 && value < _speed)
            {
                _speed = value; return true; 
            }
            else
            {
                return false;
            }
        }
    }
}