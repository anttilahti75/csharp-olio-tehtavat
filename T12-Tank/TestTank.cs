using System;

namespace T12TankApplication
{
    class TestTank
    {
        // The main program
        static void Main(string[] args)
        {
            int[] speedArray = new int[] { -10, 50, -500, 100, 200 };
            int[] crewArray = new int[] { -10, 0, 2, -4, 20 };

            Tank tank1 = new Tank
            {
                Name = "Johnson",
                Type = "Sherman"
            };

            Console.WriteLine("Name: " + tank1.Name);
            Console.WriteLine("Type: " + tank1.Type);
            Console.WriteLine("Maximum speed is: " + tank1.SpeedMax);
            Console.WriteLine("Current speed is: " + tank1.Speed);
            Console.WriteLine("Current crewnumber is: " + tank1.CrewNumber);
            
            Console.WriteLine("\nBegin speed tests\n");
            for (int i = 0; i < speedArray.Length; i++)
            {
                Console.WriteLine("Setting speed to " + speedArray[i]);
                if (speedArray[i] > tank1.Speed)
                {
                    tank1.AccelerateTo(speedArray[i]);
                }
                else
                {
                    tank1.SlowTo(speedArray[i]);
                }
                Console.WriteLine("Current speed is: " + tank1.Speed);                    
            }

            Console.WriteLine("\nBegin crew tests\n");
            for (int i = 0; i < crewArray.Length; i++)
            {
                Console.WriteLine("Setting crew to " + crewArray[i]);
                tank1.CrewNumber = crewArray[i];
                Console.WriteLine("Current crewnumber is: " + tank1.CrewNumber);                    
            }

            Console.ReadLine();
        }
    }
}