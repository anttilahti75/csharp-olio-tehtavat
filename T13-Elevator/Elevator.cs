using System;

namespace T13Elevator
{
    class Elevator
    {
        private int _maxFloor = 5;
        private int _minFloor = 1;
        private int _curFloor = 1;

        public int MaxFloor
        {
            get { return _maxFloor; }
        } 

        public int MinFloor
        {
            get { return _minFloor; }
        }

        public int CurFloor
        {
            get { return _curFloor; }
        }
        
        public string ToFloor(string value)
        {
            int t;
            bool success = Int32.TryParse(value, out t);

            if (success == true)
            {
                if (t >= _minFloor && t <= _maxFloor && t != _curFloor)
                {
                    _curFloor = t;
                    return "Elevator is now in floor: " + _curFloor;
                }
                else if (t == _curFloor)
                {
                    return "Elevator is already in that floor";
                }
                else
                {
                    return "Cannot go there";
                    }
            }
            else
            {
                return "Not a number!";
            }
        }
    }
}
