using System;

namespace T13Elevator
{
    class Program
    {
        // The main program
        static void Main(string[] args)
        {
           
            Elevator elev = new Elevator();

            Console.WriteLine("Minimum floor is " + elev.MinFloor);
            Console.WriteLine("Maximum floor is " + elev.MaxFloor);
            Console.WriteLine("Current floor is " + elev.CurFloor);

            while (true)
            {
                Console.Write("Give a new floor number (" + elev.MinFloor + "-" + elev.MaxFloor + ") > ");
                string input = Console.ReadLine();
            }

            Console.ReadLine();
        }
    }
}
