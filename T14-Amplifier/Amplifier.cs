using System;

namespace T14Amplifier
{
    class Amplifier
    {
        private int _maxVol = 100;
        private int _minVol = 0;
        private int _curVol = 0;

        public int MaxVol
        {
            get { return _maxVol; }
        } 

        public int MinVol
        {
            get { return _minVol; }
        }

        public int CurVol
        {
            get { return _curVol; }
        }
        
        public string ToVol(string value)
        {
            int t;
            bool success = Int32.TryParse(value, out t);

            if (success == true)
            {
                if (t <= _minVol)
                {
                    _curVol = 0;
                    return "Volume set to 0";
                }
                else if (t >= _maxVol)
                {
                    _curVol = 100;
                    return "Volume set to 100";
                }
                else if (t == _curVol)
                {
                    return "Volume is already that level";
                }
                else if (t >= _minVol && t <= _maxVol)
                {
                    _curVol = t;
                    return "Volume set to " + t;
                }
                else
                {
                    return "Something out of the ordinary happened!";
                }
            }
            else
            {
                return "Not a number!";
            }
        }
    }
}
