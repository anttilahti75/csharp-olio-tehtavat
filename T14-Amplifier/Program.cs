using System;

namespace T14Amplifier
{
    class Program
    {
        // The main program
        static void Main(string[] args)
        {
           
            Amplifier amp = new Amplifier();

            Console.WriteLine("Minimum volume is " + amp.MinVol);
            Console.WriteLine("Maximum volume is " + amp.MaxVol);
            Console.WriteLine("Current volume is " + amp.CurVol);

            while (true)
            {
                Console.Write("Give a new volume level (" + amp.MinVol + "-" + amp.MaxVol + ") > ");
                string input = Console.ReadLine();
                Console.WriteLine(amp.ToVol(input));
            }

            Console.ReadLine();
        }
    }
}
