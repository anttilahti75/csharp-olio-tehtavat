using System;

namespace T15Employee
{
    public class Boss : Employee
    {
        public string Car { get; set; }
        public int Bonus { get; set; }
        
        public override string ToString()
        {
            return base.ToString() + $"\nCar: {Car}\nBonus: {Bonus}";
        }
    } 
}
