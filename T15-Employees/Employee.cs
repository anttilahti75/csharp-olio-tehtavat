using System;

namespace T15Employee
{
    public class Employee
    {
        public string Name { get; set; }
        public string Profession { get; set; }
        public int Salary { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}\nProfession: {Profession}\nSalary: {Salary}";
        }
    }
}
