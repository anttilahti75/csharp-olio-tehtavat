using System;

namespace T15Employee
{
    class Program
    {
        static void Main(string[] args)
        {

            Employee emp1 = new Employee
            {
                Name = "Matti Meikäläinen",
                Profession = "Developer",
                Salary = 3000
            };

            Boss boss1 = new Boss
            {
                Name = "Maija Meikäläinen",
                Profession = "TJ",
                Salary = 4000,
                Car = "Skoda",
                Bonus = 1000
            };

            Console.WriteLine(emp1.ToString());
            Console.WriteLine("\n" + boss1.ToString());
            emp1.Profession = "Lead developer";
            emp1.Salary = 3500;
            Console.WriteLine("\n" + emp1.ToString());

            Console.ReadLine();
        }
    }
}
