using System;

namespace T16Vehicles
{
    public class Bike : Vehicle
    {
        public bool Gears { get; set; }
        public string GearModel { get; set; }

        public override string ToString()
        {
            return base.ToString() + $", Gears: {Gears}, Gear Model: {GearModel}";
        }
    }
}
