using System;

namespace T16Vehicles
{
    public class Boat : Vehicle
    {
        public string Type { get; set; }
        public int Seats { get; set; }

        public override string ToString()
        {
            return base.ToString() + $", Type: {Type}, Seats: {Seats}";
        }
    }
}
