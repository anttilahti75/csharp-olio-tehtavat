using System;

namespace T16Vehicles
{
    class Program
    {
        static void Main(string[] args)
        {
            Vehicle bike1 = new Bike
            {
                Name = "Helkama",
                Model = "Jopo",
                Year = 2000,
                Colour = "Red",
                Gears = false,
                GearModel = ""
            };

            Vehicle boat1 = new Boat
            {
                Name = "Yamaha",
                Model = "Kiitäjä",
                Year = 2010,
                Colour = "White",
                Type = "rowboat",
                Seats = 4
            };

            Vehicle bike2 = new Bike
            {
                Name = "Kulkija",
                Model = "Poljin",
                Year = 1991,
                Colour = "Green",
                Gears = true,
                GearModel = "Shimano"
            };

            Vehicle boat2 = new Boat
            {
                Name = "Paatti",
                Model = "Renault",
                Year = 2020,
                Colour = "Brown",
                Type = "motorboat",
                Seats = 2
            };

            Console.WriteLine("Bike #1: " + bike1.ToString());
            Console.WriteLine("Bike #2: " + bike2.ToString());
            Console.WriteLine("Boat #1: " + boat1.ToString());
            Console.WriteLine("Boat #2: " + boat2.ToString());

            Console.ReadLine();
        }
    }
}
