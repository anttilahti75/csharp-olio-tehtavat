using System;

namespace T16Vehicles
{
    public class Vehicle
    {
        public string Name { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string Colour { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}, Model: {Model}, Year: {Year}, Colour: {Colour}";
        }
    }
}
