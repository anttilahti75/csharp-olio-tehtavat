using System;

namespace T17Radio
{
    class Program
    {
        static void Main(string[] args)
        {
            Radio radio = new Radio();

            Console.WriteLine("Radio info:\n" + radio.ToString());
            Console.WriteLine(radio.ModifyVolume(3));
            Console.WriteLine(radio.ModifyFreq(18000.0f));
            Console.WriteLine(radio.TogglePower());
            Console.WriteLine(radio.ModifyVolume(3));
            Console.WriteLine(radio.ModifyFreq(18000.0f));
            Console.WriteLine(radio.ModifyVolume(-10));
            Console.WriteLine(radio.ModifyFreq(38000.0f));
            Console.WriteLine("Radio info:\n" + radio.ToString());

            Console.ReadLine();
        }
    }
}
