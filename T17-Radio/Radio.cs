using System;

namespace T17Radio
{
    public class Radio
    {
        public bool Power = false;
        public int Volume = 0;
        public float Freq = 12000.0f;
        private int _volMax = 9;
        private int _volMin = 0;
        private float _freqMax = 26000.0f;
        private float _freqMin = 2000.0f;

        public string TogglePower()
        {
            if (Power == false)
            {
                Power = true;
                return "Powered on";
            }
            else
            {
                Power = false;
                return "Powered off";
            }
        }

        public string ModifyVolume(int value)
        {
            if (Power == false)
            {
                return "Cannot change Volume, Power is off";
            }
            else if (value >= _volMin && value <= _volMax)
            {
                Volume = value;
                return $"Setting Volume to {Volume}";
            }
            else
            {
                return $"Invalid value for Volume: {value}";
            }
        }

        public string ModifyFreq(float value)
        {
            if (Power == false)
            {
                return "Cannot change frequency, Power is off";
            }
            else if (value >= _freqMin && value <= _freqMax)
            {
                Freq = value;
                return $"Setting frequency to {Freq}";
            }
            else
            {
                return $"Invalid value for frequency: {value}";
            }
        }

        public override string ToString()
        {
            return $"Is powered? {Power}, Volume: {Volume} ({_volMin} - {_volMax}), Frequency: {Freq} ({_freqMin} - {_freqMax})";
        }
    }
}
