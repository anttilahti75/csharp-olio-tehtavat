using System;

namespace T18Tavarat
{
    public class Asset
    {
        public string Name { get; set; }
        public float Price { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}\nPrice: {Price}";
        }
    }
}
