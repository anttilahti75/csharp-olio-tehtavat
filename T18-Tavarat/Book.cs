using System;

namespace T18Tavarat
{
    public class Book : Media
    {
        public int Pages { get; set; }
        public bool Read { get; set; }
    
        public override string ToString()
        {
            return base.ToString() + $"\nPages: {Pages}\nRead: {Read}"; 
        }
    }

}
