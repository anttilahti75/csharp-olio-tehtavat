using System;

namespace T18Tavarat
{
    public class CD : Media
    {
        public bool Listened { get; set; }
        public int Songs { get; set; }
        
        public override string ToString()
        {
            return base.ToString() + $"\nListened: {Listened}\nSongs: {Songs}";
        }
    }

}
