using System;

namespace T18Tavarat
{
    public class Electronic : Asset
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public bool Power { get; set; }
    
        public override string ToString()
        {
            return base.ToString() + $"\nBrand: {Brand}\nModel: {Model}\nPowered: {Power}"; 
        }
    }

}
