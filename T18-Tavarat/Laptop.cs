using System;

namespace T18Tavarat
{
    public class Laptop : Electronic
    {
        public bool OpticalDrive { get; set; }

        public override string ToString()
        {
            return base.ToString() + $"\nOptical Drive: {OpticalDrive}"; 
        }
    }
}
