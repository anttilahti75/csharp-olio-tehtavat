using System;

namespace T18Tavarat
{
    public class Media : Asset
    {
        public string Genre { get; set; }
    
        public override string ToString()
        {
            return base.ToString() + $"\nGenre: {Genre}"; 
        }
    }

}
