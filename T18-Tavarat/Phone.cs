using System;

namespace T18Tavarat
{
    public class Phone : Electronic
    {
        public int Contacts { get; set; }

        public override string ToString()
        {
            return base.ToString() + $"\nContacts: {Contacts}"; 
        }
    }
}
