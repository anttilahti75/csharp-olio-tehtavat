using System;

namespace T18Tavarat
{
    class Program
    {
        static void Main(string[] args)
        {
            CD cd1 = new CD
            {
                Name = "Top Hits",
                Price = 19.90f,
                Genre = "Pop",
                Listened = false,
                Songs = 10
            };

            Book book1 = new Book
            {
                Name = "Soturikissat",
                Price = 29.90f,
                Genre = "Fantasy",
                Pages = 300,
                Read = false
            };

            Phone phone1 = new Phone
            {
                Name = "Luuri",
                Price = 299.90f,
                Brand = "Nokia",
                Model = "4.2",
                Power = true,
                Contacts = 10
            };

            Laptop laptop1 = new Laptop
            {
                Name = "Läppäri",
                Price = 799.90f,
                Brand = "Lenovo",
                Model = "t450s",
                Power = false,
                OpticalDrive = false
            };

            Console.WriteLine("CD info:\n" + cd1.ToString());
            Console.WriteLine("\nBook info:\n" + book1.ToString());
            Console.WriteLine("\nPhone info:\n" + phone1.ToString());
            Console.WriteLine("\nLaptop info:\n" + laptop1.ToString());

            Console.ReadLine();
        }
    }
}
