using System;

namespace T2MakihyppyApplication
{
    class Makihyppy
    {
        static void Main(string[] args)
        {

            // Variable declarations
            int total = 0;
            int userPoint = 0;
            int arrayIndex = 0;
            int[] pointsArray = new int[5];

            // Ask user input
            do 
            {
                Console.Write("Give points: ");
                string input = Console.ReadLine();

                // Check if input is number
                bool success = Int32.TryParse(input, out userPoint);

                if (success == true)
                {
                    pointsArray[arrayIndex] = userPoint;
                    arrayIndex++;
                }
                else
                {
                    Console.WriteLine("You did not enter a number!");
                }
            } 
            while (arrayIndex < pointsArray.Length);

            // Array before sorting
            Console.WriteLine("Before sorting");
            for (int a = 0; a < pointsArray.Length; a++)
            {
                Console.WriteLine(pointsArray[a]);
            }

            // Sort the array and set first (lowest) and last (highest) to zero
            Array.Sort(pointsArray);
            pointsArray[0] = 0;
            pointsArray[pointsArray.Length - 1] = 0;

            // Array after sorting
            Console.WriteLine("After sorting and removing lowest and highest");
            for (int a = 0; a < pointsArray.Length; a++)
            {
                Console.WriteLine(pointsArray[a]);
                total += pointsArray[a];
            }

            Console.WriteLine("Total points are " + total);
            Console.ReadLine();
        }
    }
}