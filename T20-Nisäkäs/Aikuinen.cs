using System;

namespace T20Nisakas
{
    class Aikuinen : Ihminen
    {
            
        public string Auto { get; set; }

        public override string Liiku()
        {
            return "kävelee";
        }

        public override string ToString()
        {
            return base.ToString() + $", auto: {Auto}";
        }
    }
}
