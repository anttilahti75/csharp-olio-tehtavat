using System;

namespace T20Nisakas
{
    public class Ihminen : Nisakas
    {
        private int _ika;
        public int Paino;
        public int Pituus;
        public string Nimi;

        public override int Ika
        {
            get { return _ika; }
            set { _ika = value; }
        }
        public override string Liiku()
        {
            return "liikkuu";
        }
        public int Kasva()
        {
            _ika += 1;
            return _ika;
        }

        public override string ToString()
        {
            return $"Ikä: {_ika}, paino: {Paino}, pituus: {Pituus}, nimi: {Nimi}";
        }
    }
}
