using System;

namespace T20Nisakas
{
    public abstract class Nisakas
    {
        public abstract int Ika { get; set; }

        public abstract string Liiku();
    }
}
