using System;

namespace T20Nisakas
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Ihminen i1 = new Ihminen
            {
                Ika = 30,
                Paino = 75,
                Pituus = 185,
                Nimi = "Hessu Hopo"
            };

            Ihminen a1 = new Aikuinen
            {
                Ika = 18,
                Paino = 70,
                Pituus = 180,
                Nimi = "Matti Meikäläinen",
                Auto = "Skoda"
            };

            Ihminen v1 = new Vauva
            {
                Ika = 3,
                Paino = 3,
                Pituus = 40,
                Nimi = "Kirsi Kernel",
                Vaippa = "Päällä"
            };

            Console.WriteLine("\n" + i1.ToString());
            Console.WriteLine($"{i1.Nimi} {i1.Liiku()}");
            i1.Kasva();
            Console.WriteLine(i1.ToString());

            Console.WriteLine("\n" + a1.ToString());
            Console.WriteLine($"{a1.Nimi} {a1.Liiku()}");
            a1.Kasva();
            Console.WriteLine(a1.ToString());

            Console.WriteLine("\n" + v1.ToString());
            Console.WriteLine($"{v1.Nimi} {v1.Liiku()}");
            v1.Kasva();
            Console.WriteLine(v1.ToString());

            Console.ReadLine();
        }
    }
}
