using System;

namespace T20Nisakas
{
    public class Vauva : Ihminen
    {
        public string Vaippa { get; set; }

        public override string Liiku()
        {
            return "konttaa";
        }

        public override string ToString()
        {
            return base.ToString() + $", vaippa: {Vaippa}";
        }
    }
}
