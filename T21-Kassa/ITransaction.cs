using System;

namespace T21Kassa
{
    public interface ITransaction
    {
        // interface members 
        string ShowTransaction(); 
        float GetAmount();
    }
}
