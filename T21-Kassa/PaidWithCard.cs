using System;

namespace T21Kassa
{
    class PaidWithCard : ITransaction
    {
        private float _card;
        private float _lastCard;
        private int _transaction = 0;

        DateTime thisDay = DateTime.Today;

        public void ChangeCard(float value)
        {
            if (value != 0)
            { 
                _card += value;
                _lastCard = value;
                _transaction += 1;
            };
        }
        public string ShowTransaction()
        {
            string date = thisDay.ToString("dd/MM/yyyy");
            return $"Transaction to bank: charge from card {FancyCardReader()}, date {date}";
        }
        
        public string FancyCardReader()
        {
            Random rnd = new Random();
            return rnd.Next(10000) + "-" + rnd.Next(10000);
        }

        public float GetAmount()
        {
            return _lastCard;
        }
        
        public float ShowCard()
        {
            return _card;
        }
    }
}

