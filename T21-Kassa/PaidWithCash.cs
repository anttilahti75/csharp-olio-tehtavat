using System;

namespace T21Kassa
{
    class PaidWithCash : ITransaction
    {
        private float _cash;
        private float _lastCash;
        private int _transaction;

        DateTime thisDay = DateTime.Today;

        public void ChangeCash(float value)
        {
            if (value != 0)
            { 
                _cash += value; 
                _lastCash = value;
                _transaction += 1;
            };
        }
        public string ShowTransaction()
        {
            string date = thisDay.ToString("dd/MM/yyyy");
            return $"Paid with cash: billnumber {_transaction}, date {date}";
        }
        
        public float GetAmount()
        {
            return _lastCash;
        }
        
        public float ShowCash()
        {
            return _cash;
        }
    }
}
