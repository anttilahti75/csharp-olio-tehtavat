using System;

namespace T21Kassa
{
    class Program
    {
        static void Main(string[] args)
        {
            PaidWithCash cash = new PaidWithCash();
            PaidWithCard card = new PaidWithCard();

            DateTime thisDay = DateTime.Today;

            Console.WriteLine("Total money in cash: " + cash.ShowCash());
            cash.ChangeCash(10.0f);
            Console.WriteLine(cash.ShowTransaction() + ", amount: " + cash.GetAmount());
            Console.WriteLine("Total money in cash: " + cash.ShowCash());
            
            Console.WriteLine("Total money at bank account: " + card.ShowCard());
            card.ChangeCard(15.9f);
            Console.WriteLine(card.ShowTransaction() + ", amount: " + card.GetAmount());
            Console.WriteLine("Total money at bank account: " + card.ShowCard());

            float total = cash.ShowCash() + card.ShowCard();

            Console.WriteLine("Total sales today, " + thisDay.ToString("D") + ", is " + total);

            Console.ReadLine();
        }
    }
}
