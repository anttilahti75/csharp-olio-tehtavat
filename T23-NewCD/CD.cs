using System;
using System.Linq;
using System.Collections.Generic;

namespace T23NewCD
{
    public class CD
    {
        public string cdName;
        public string artist;
        private int _songCount;
        private string _cdLength = "0";
        private List<object> _songs = new List<object>();

        public int songCount
        {
            get { return _songCount; }
        }

        public string cdLength
        {
            get { return _cdLength; }
        }

        public void ListSongs()
        {
            foreach (Song song in _songs)
            {
                Console.WriteLine(song.ToString());
            }
        }
        public string AddSong(string argSongName, string argSongLength)
        {
            if (TestSecFormat(argSongLength) == false)
            {
                return "Error! Too many seconds characters!";
            }

            if (TestTypes(argSongLength) == false)
            {
                return "Error! Enter numbers!";
            }
            
            var tuple = SongLengthToInt(argSongLength);

            if (TestSeconds(tuple.sec) == false)
            {
                return "Error! Seconds value too big!";
            }

            _songs.Add(new Song
            { 
                songName = argSongName,
                songLength = argSongLength
            });

            _songCount = _songs.Count;

            return "Song added";
        }

        // Test if array indices are integers
        private bool TestTypes(string value)
        {
            string[] str = value.Split(":");
            
            int errors = 0;
            for (int i = 0; i < str.Length; i++)
            {
                int t = 0;
                bool success = Int32.TryParse(str[i], out t);
                if (success != true)
                {
                    errors += 1;
                }
            }
            if (errors > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
            
        }
        
        // Test if there are too many numbers in the seconds field
        private bool TestSecFormat(string value)
        {
            string[] str = value.Split(":");
            if (str[1].Length > 2)
            {
                return false;
            }
            for (int i = 0; i < str.Length; i++)
            {
                if (str[1].Length > 2 || str[1].Length < 2)
                {
                    return false;
                }
            }
            return true;
        }
        
        // Test if seconds count over 60
        private bool TestSeconds(int value)
        {
            if (value >= 60)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        // Calculate song length
        private int SongLengthToSeconds(string value)
        {
            var tuple = SongLengthToInt(value);
            //Console.WriteLine(tuple.min + " " + tuple.sec);
            int t = tuple.min * 60 + tuple.sec;
            //Console.WriteLine("Song length in secods: " + t);
            return t;
        }
        
        // Convert song length from string to integers
        private (int min, int sec) SongLengthToInt(string value)
        {
            string[] str = value.Split(":");
            int[] num = new int[str.Length];
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] != null)
                { 
                    num[i] = Int32.Parse(str[i]);
                }
                else num[i] = 0;
            }
            Array.Reverse(num);
            int min = num[1];
            int sec = num[0];
            return (min, sec);
        }

        // Calculate the total length of all songs on a cd
        private void CalcCDLength(){
            int sec = 0;
            foreach (Song song in _songs)
            {
                sec += SongLengthToSeconds(song.songLength);
            }
            int min = sec / 60;
            sec = sec % 60;
            //Console.WriteLine($"Min: {min}, sec: {sec:D2}");
            _cdLength = $"{min}:{sec:D2}";
        }
        

        public override string ToString()
        {
            CalcCDLength();
            // str info = ...
            // info += {var}
            // return info
            return $"- name: {cdName}\n- artist: {artist}\n- total length: {_cdLength}\n- {_songCount} songs:";
        }
    }
}
