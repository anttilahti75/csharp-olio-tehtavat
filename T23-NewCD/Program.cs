using System;

namespace T23NewCD
{
    class Program
    {
        static void Main(string[] args)
        {
            
            CD cd1 = new CD
            {
                cdName = "Endless Forms Most Beautiful",
                artist = "Nightwish"
            };

            cd1.AddSong("Shudder Before the Beautiful","6:29");
            cd1.AddSong("Weak Fantasy","5:23");
            cd1.AddSong("Elan","4:45");
            cd1.AddSong("Yours Is an Empty Hope","5:34");
            cd1.AddSong("Our Decades in the Sun","6:37");
            cd1.AddSong("My Walden","4:38");
            cd1.AddSong("Endless Forms Most Beautiful","5:07");
            cd1.AddSong("Edema Ruh","5:15");
            cd1.AddSong("Alpenglow","4:45");
            cd1.AddSong("The Eyes of Sharbat Gula","6:03");
            cd1.AddSong("The Greatest Show on Earth","24:00");
            Console.WriteLine("\nYou have a CD:");
            Console.WriteLine(cd1.ToString());
            cd1.ListSongs();

            CD cd2 = new CD
            {
                cdName = "Gomorrah",
                artist = "Gomorrah"
            };

            cd2.AddSong("Ember", "3:51");
            cd2.AddSong("From Earthen Ruins","3:43");
            cd2.AddSong("Predatory Reich","2:25");
            cd2.AddSong("Frailty","2:39");
            cd2.AddSong("For Those of Eld","3:38");
            cd2.AddSong("The Carnage Wrought","3:45");
            cd2.AddSong("The Blade Itself","2:24");
            cd2.AddSong("Of Ghosts and the Grave","3:54");
            Console.WriteLine("\nYou have a CD:");
            Console.WriteLine(cd2.ToString());
            cd2.ListSongs(); 
            
            CD cd3 = new CD
            {
                cdName = "CD3",
                artist = "artist 3"
            };

            cd3.AddSong("Song #1","1:30");
            cd3.AddSong("Song #2","1:30");
            cd3.AddSong("Song #3","2:45");
            cd3.AddSong("Song #4","3:13");
            Console.WriteLine("\nYou have a CD:");
            Console.WriteLine(cd3.ToString());
            cd3.ListSongs();

            Console.ReadLine();
        }    
    }
}
