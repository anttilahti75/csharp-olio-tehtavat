using System;

namespace T23NewCD
{
    public class Song
    {
        public string songName { get; set; }
        public string songLength { get; set; }

        public override string ToString()
        {
            return $"  - {songName}, {songLength}";
        }
    }
}
