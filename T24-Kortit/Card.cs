using System;

namespace T24Kortit
{
    public class Card
    {
        public string Name { get; set; }
        public string Colour { get; set; }
        public bool IsInDeck { get; set; }

        public override string ToString()
        {
            return $"name: {Name}, colour: {Colour}, Is in deck: {IsInDeck}";
        }
    }
}