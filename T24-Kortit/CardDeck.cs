using System;
using System.Linq;
using System.Collections.Generic;

namespace T24Kortit
{
    public class CardDeck
    {
        private string[] _colours = { "Hearts", "Spades", "Diamonds", "Suits" };
        private List<object> _cards = new List<object>();
        private string[] _cardList = { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };

        public string PrepareDeck()
        {
            for (int i = 0; i < _colours.Length; i++)
            {
                for (int c = 0; c < _cardList.Length; c++)
                {
                    _cards.Add(new Card
                    {
                        Name = _cardList[c],
                        Colour = _colours[i],
                        IsInDeck = true
                    });
                }
            }
            return "Deck prepared";
        }
        public void ListCards()
        {
            foreach (Card card in _cards)
            {
                Console.WriteLine(card.ToString());
            }
        }

        public void ShuffleDeck()
        {
            Random rand = new Random();

            // For each spot in the array, pick
            // a random item to swap into that spot.
            // http://csharphelper.com/blog/2014/07/randomize-arrays-in-c/
            for (int i = 0; i < _cards.Count - 1; i++)
            {
                int j = rand.Next(i, _cards.Count);
                object temp = _cards[i];
                _cards[i] = _cards[j];
                _cards[j] = temp;
            }
            Console.WriteLine("Deck shuffled");
        }
    }
}