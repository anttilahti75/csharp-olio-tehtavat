using System;

namespace T24Kortit
{
    class Program
    {
        static void Main(string[] args)
        {
            CardDeck deck = new CardDeck();

            Console.WriteLine(deck.PrepareDeck());
            deck.ListCards();
            deck.ShuffleDeck();
            deck.ListCards();

            Console.ReadLine();
        }
    }
}