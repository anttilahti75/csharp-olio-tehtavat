/*
using System;
using System.Linq;
using System.Collections.Generic;

namespace T24Kortit {
    public class Shuffler {
        private string[] Colours = { "Hearts", "Spades", "Diamonds", "Suits" };

        private List<object> CardDecks = new List<object>();

        private List<object> SingleDeck = new List<object>();

        public string CreateDecks(){
            for (int d = 0; d < Colours.Length; d++) {
                CardDecks.Add(new CardDeck {
                    DeckColour = Colours[d]
                });
            }
            return "Decks created";
        }
        public string AddCardsToDecks(){
            foreach (CardDeck cardDeck in CardDecks) {
                cardDeck.PrepareDeck();
            }
            return "Cards are in decks";
        }

        public void ListCardsInDecks(){
            foreach (CardDeck cardDeck in CardDecks) {
                Console.WriteLine("Deck: " + cardDeck.DeckColour);
                cardDeck.ListCards();
            }
        }

        public void JoinDecks(){
            foreach (CardDeck cardDeck in CardDecks) {
                SingleDeck.Add(cardDeck);
            }
        }

        public void ListCardsInSingleDeck(){
            foreach (CardDeck cardDeck in SingleDeck) {
                cardDeck.ListCards();
            }
        }
        public void ShuffleDeck(){
            Random rand = new Random();

            // For each spot in the array, pick
            // a random item to swap into that spot.
            for (int i = 0; i < SingleDeck.Count - 1; i++) {
                int j = rand.Next(i, SingleDeck.Count);
                object temp = SingleDeck[i];
                SingleDeck[i] = SingleDeck[j];
                SingleDeck[j] = temp;
            }
            Console.WriteLine("Deck shuffled");
        }
    }
}
*/