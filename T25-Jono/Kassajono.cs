using System;
using System.Collections.Generic;

namespace T25Jono
{
    public class Kassajono
    {
        private Queue<string> _jono = new Queue<string>();

        public string MeneJonoon(string value)
        {
            _jono.Enqueue(value);
            return $"Jonossa on nyt {_jono.Count} asiakasta";
        }

        public void PoistaYksiJonosta()
        {
            if (_jono.Count == 0)
            {
                Console.WriteLine("Jono on tyhjä");
            }
            else
            {
                Console.WriteLine("--- Palvellaan jonon ensimmäinen asiakas ---");
                Console.WriteLine("Palvelen nyt asiakasta: {0}", _jono.Dequeue());
            }
        }

        public void NaytaJono()
        {
            foreach(var asiakas in _jono)
            {
                Console.WriteLine(asiakas);
            }

        }
        
        public void PoistaJonosta()
        {
			foreach(var asiakas in _jono)
            {
                Console.WriteLine("Palvelen nyt asiakasta: {0}", _jono.Dequeue());
            }
		}
    }
}
