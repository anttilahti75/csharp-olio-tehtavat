using System;

namespace T25Jono
{
    class Program
    {
        static void Main(string[] args)
        {
            Kassajono kassa1 = new Kassajono();
            
            while (true)
            {
                Console.Write("Anna asiakkaan nimi (enter lopettaa): ");

                string input = Console.ReadLine();

                if (String.IsNullOrEmpty(input) == true)
                {
                    break;
                }
                else
                {
                    Console.WriteLine(kassa1.MeneJonoon(input));
                }
            }

            Console.WriteLine("\nAsiakkaiden palvelu\n");
            kassa1.PoistaJonosta();

            while (true)
            {
                Console.Write("Enter poistaa asiakkaan (Q lopettaa): ");

                string input = Console.ReadLine();

                if (input == "Q")
                {
                    break;
                }
                else
                {
                    kassa1.PoistaYksiJonosta();
                }
            }
            
            Console.ReadLine();
        }
    }
}
