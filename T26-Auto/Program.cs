using System;
using System.Collections.Generic;

namespace T26Auto
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Auto> Autot = new List<Auto>();

            Auto auto1 = new Auto(){Merkki = "Skoda", Malli = "Felicia", MaxRenkaidenLkm = 4};

            auto1.LisaaRengas("Nokia", "Hakkapeliitta", "205/45R16");
            auto1.LisaaRengas("Nokia", "Hakkapeliitta", "205/45R16");
            auto1.LisaaRengas("Nokia", "Hakkapeliitta", "205/45R16");
            auto1.LisaaRengas("Nokia", "Hakkapeliitta", "205/45R16");
            Autot.Add(auto1);

            Auto auto2 = new Auto(){Merkki = "Datsun", Malli = "100A", MaxRenkaidenLkm = 4};

            auto2.LisaaRengas("GoodYear", "Excellence", "180/75R14");
            auto2.LisaaRengas("GoodYear", "Excellence", "180/75R14");
            auto2.LisaaRengas("GoodYear", "Excellence", "180/75R14");
            auto2.LisaaRengas("GoodYear", "Excellence", "180/75R14");
            Autot.Add(auto2);

            Auto auto3 = new Auto(){Merkki = "Volvo", Malli = "FM 10x4 Rigid", MaxRenkaidenLkm = 10};

            auto3.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            auto3.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            auto3.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            auto3.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            auto3.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            auto3.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            auto3.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            auto3.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            auto3.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            auto3.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            Autot.Add(auto3);

            foreach(Auto auto in Autot)
            {
                Console.WriteLine("\n" + auto);
                Console.WriteLine("Renkaat:");
                auto.NaytaRenkaat();
            }

            Console.ReadLine();
        }
    }
}