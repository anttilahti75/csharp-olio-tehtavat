using System;
using System.Collections.Generic;

namespace T27Kulkuneuvot
{
    public class Kulkuneuvo
    {
        public string Merkki { get; set; }
        public string Malli { get; set; }
        public int RenkaidenLkm
        { 
            get { return Renkaat.Count; }
        }
        public int MaxRenkaidenLkm { get; set; }
        public List<Rengas> Renkaat { get; set; } = new List<Rengas>();

        public void LisaaRengas(string valmistaja, string malli, string rengaskoko)
        {
            if (Renkaat.Count < MaxRenkaidenLkm)
            {
                Renkaat.Add(new Rengas
                {
                    Valmistaja = valmistaja,
                    Malli = malli,
                    Rengaskoko = rengaskoko
                });
            }
            else { Console.WriteLine("Enempää renkaita ei mahdu."); }
        }

        public void NaytaRenkaat()
        {
            foreach (Rengas rengas in Renkaat)
            {
                Console.WriteLine(rengas.ToString());
            }
        }

        public override string ToString()
        {
            return $"Merkki: {Merkki}, Malli: {Malli}, Renkaita: {RenkaidenLkm}, Renkaita max: {MaxRenkaidenLkm}";
        }
    }
}