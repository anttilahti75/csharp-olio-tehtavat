using System;
using System.Collections.Generic;

namespace T27Kulkuneuvot
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Kulkuneuvo> Kulkuneuvot = new List<Kulkuneuvo>();

            Kulkuneuvo auto = new Kulkuneuvo(){Merkki = "Skoda", Malli = "Felicia", MaxRenkaidenLkm = 4};

            auto.LisaaRengas("Nokia", "Hakkapeliitta", "205/45R16");
            auto.LisaaRengas("Nokia", "Hakkapeliitta", "205/45R16");
            auto.LisaaRengas("Nokia", "Hakkapeliitta", "205/45R16");
            auto.LisaaRengas("Nokia", "Hakkapeliitta", "205/45R16");
            Kulkuneuvot.Add(auto);

            Kulkuneuvo mopo = new Kulkuneuvo(){Merkki = "Honda Monkey", Malli = "Z50A", MaxRenkaidenLkm = 2};

            mopo.LisaaRengas("Heidenau", "K75", "3.50-8");
            mopo.LisaaRengas("Heidenau", "K75", "3.50-8");
            Kulkuneuvot.Add(mopo);

            Kulkuneuvo rekka = new Kulkuneuvo(){Merkki = "Volvo", Malli = "FM 10x4 Rigid", MaxRenkaidenLkm = 10};

            rekka.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            rekka.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            rekka.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            rekka.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            rekka.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            rekka.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            rekka.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            rekka.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            rekka.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            rekka.LisaaRengas("Pirelli", "ST01", "385/65R22.5");
            Kulkuneuvot.Add(rekka);

            foreach(Kulkuneuvo kulkuneuvo in Kulkuneuvot)
            {
                Console.WriteLine("\n" + kulkuneuvo);
                Console.WriteLine("Renkaat:");
                kulkuneuvo.NaytaRenkaat();
            }

            Console.ReadLine();
        }
    }
}