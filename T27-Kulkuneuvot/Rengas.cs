using System;

namespace T27Kulkuneuvot
{
    public class Rengas
    {
        public string Valmistaja { get; set; }
        public string Malli { get; set; }
        public string Rengaskoko { get; set; }

        public override string ToString()
        {
            return $"valmistaja: {Valmistaja}, malli: {Malli}, rengaskoko: {Rengaskoko}";
        }
    }
}