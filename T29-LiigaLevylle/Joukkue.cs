using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace T29LiigaLevylle
{
    public class Joukkue
    {
        public string Nimi { get; set; }
        public string Kotikaupunki { get; set; }
        public List<Pelaaja> Pelaajat { get; } = new List<Pelaaja>();

        private void HaePelaajat(string joukkue)
        {
            Console.WriteLine("Haetaan pelaajat.");
            try
            {
                string filename = $"{joukkue}.csv";
                string[] lines = System.IO.File.ReadAllLines(filename);
                foreach (string line in lines)
                {
                    string[] str = line.Split(";");
                    string etunimi = str[0];
                    string sukunimi = str[1];
                    string pelipaikka = str[2];
                    int pelinumero = Int32.Parse(str[3]);

                    Pelaajat.Add(new Pelaaja(etunimi, sukunimi, pelipaikka, pelinumero));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private bool Tallennus(List<Pelaaja> list, string joukkue)
        {
            string filename = $"{joukkue}.csv";
            try
            {
                using (StreamWriter sw = new StreamWriter(filename))
                {
                    foreach (Pelaaja pelaaja in list)                    
                    {
                        sw.WriteLine($"{pelaaja.Etunimi};{pelaaja.Sukunimi};{pelaaja.Pelipaikka};{pelaaja.Pelinumero}");
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }

        public void NäytäPelaajat()
        {
            foreach (Pelaaja p in Pelaajat)
            {
                Console.WriteLine(p.ToString());
            }
        }
        
        public bool LisääPelaaja(Pelaaja p)
        {
            Pelaajat.Add(p);
            return true;
        }

        public override string ToString()
        {
            return $"Joukkue: {Nimi}, kotikaupunki: {Kotikaupunki}";
        }

        public Joukkue(string nimi, string kotikaupunki)
        {
            Nimi = nimi;
            Kotikaupunki = kotikaupunki;
            HaePelaajat(nimi);
        }

        ~Joukkue()
        {
            Tallennus(Pelaajat, Nimi);
        }
    }
}