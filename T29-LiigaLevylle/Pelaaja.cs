using System;

namespace T29LiigaLevylle
{
    public class Pelaaja
    {
        public string Etunimi { get; set; }
        public string Sukunimi { get; set; }
        public string Pelipaikka { get; set; }
        public int Pelinumero { get; set; }

        public Pelaaja(string etunimi, string sukunimi, string pelipaikka, int pelinumero)
        {
            Etunimi = etunimi;
            Sukunimi = sukunimi;
            Pelipaikka = pelipaikka;
            Pelinumero = pelinumero;
        }

        public override string ToString()
        {
            return $"Etunimi: {Etunimi}, sukunimi: {Sukunimi}, pelipaikka: {Pelipaikka}, pelinumero: {Pelinumero}";
        }
    }
}