using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace T29LiigaLevylle
{
    class Program
    {
        static void Main(string[] args)
        {
            Joukkue j = new Joukkue("Hurrikaanit", "Jyväskylä");
            Console.WriteLine(j.ToString());
            Console.WriteLine("\nJoukkueen tiedostolta haetut pelaajat:\n");
            j.NäytäPelaajat();

            int pelaajienMäärä = 6;
            int etunimenMinPituus = 3;
            int etunimenMaxPituus = 8;
            int sukunimenMaxPituus = 10;
            int sukunimenMinPituus = 3;

            Console.WriteLine("\nLuodaan satunnaisia pelaajia:\n");
            for (int i = 0; i < pelaajienMäärä; i++)
            {
                j.LisääPelaaja(LuoPelaaja(etunimenMinPituus, etunimenMaxPituus, sukunimenMinPituus, sukunimenMaxPituus));
            }
            j.NäytäPelaajat();
        }

        static string RandomName(int minValue, int maxValue)
        {
            // Define allowed characters.
            char[] characters = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
            // Initialize random generator and target string.
            Random rand = new Random();
            string str = "";

            int length = rand.Next(minValue, maxValue);

            // Fetch a random character and append it str.
            for (var i = 0; i < length; i++)
            {
                int j = rand.Next(0, characters.Length);
                str += characters[j];
            }
            str = char.ToUpper(str[0]) + str.Substring(1);
            return str;
        }

        static Pelaaja LuoPelaaja(int etunimenMinPituus, int etunimenMaxPituus, int sukunimenMinPituus, int sukunimenMaxPituus)
        {
            Random rand = new Random();

            string etunimi = "";
            string sukunimi = RandomName(sukunimenMinPituus, sukunimenMaxPituus);
            string pelipaikka = "";
            int pelinumero = 0;
            int t = rand.Next(1, 6);
            switch(t)
            {
                case 1:
                case 2:
                case 3: pelipaikka = "Hyökkääjä";
                    break;
                case 4:
                case 5: pelipaikka = "Puolustaja";
                    break;
                case 6: pelipaikka = "Maalivahti";
                    break;
            }
            t = rand.Next(1, 100);
            if (t == 99)
            {
                Console.WriteLine("\n.*' WAYNE GRETZKY!!! '*. \n");
                System.Threading.Thread.Sleep(3000);
                etunimi = "Wayne";
                sukunimi = "Gretzky";
                pelipaikka = "Hyökkääjä";
                pelinumero = 99;
            } 
            else 
            {
                pelinumero = t;
                etunimi = RandomName(etunimenMinPituus, etunimenMaxPituus);
            }
            Pelaaja p = new Pelaaja(etunimi, sukunimi, pelipaikka, pelinumero);
            return p;
        }
    }
}