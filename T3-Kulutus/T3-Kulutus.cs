using System;

namespace T3KulutusApplication
{
   class Kulutus
   {
        private static Tuple<double, double> FindUsage(double dist)
        {
            // Local variable declaration
            double gasPrice = 1.55;
            double consume = 7.5;

            // Do some calculations
            double usage = dist * consume;
            double total = usage * gasPrice;

            // Return results
            var res = new Tuple<double, double>(usage, total);
            return res;
        }

        // The main program
        static void Main(string[] args)
        {
            // Declare variable(s)
            double distance = 0;

            // Ask user input
            while (true)
            {
                Console.Write("Give distance: ");
                string input = Console.ReadLine();
                bool success = double.TryParse(input, out distance);

                if (success == true)
                {
                    // Create a new function object and pass in user input
                    var r = FindUsage(distance);

                    // Display results
                    Console.WriteLine("Gasoline consume is " + r.Item1 + " liters and cost is " + r.Item2 + " euros");
                    Console.ReadLine();
                    break;
                }
                else
                {
                    Console.WriteLine("You did not enter a number!");
                }
            }
        }
    }
}