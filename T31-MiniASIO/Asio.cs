using System;
using System.Collections.Generic;
using System.Linq;

namespace T31MiniASIO
{
    public class Asio
    {
        private List<Opiskelija> opiskelijat = new List<Opiskelija>();

        public void Begin()
        {
            string input = "";
            
            do
            {
                Valikko();
                Console.Write("Anna valinta: ");
                input = Console.ReadLine();

                switch (input)
                {
                    case "L":
                    {
                        Console.WriteLine(LisääOpiskelija());
                        Console.WriteLine($"miniASIOssa on nyt {opiskelijat.Count} opiskelijaa");
                        break;
                    }
                    case "N":
                    {
                        NäytäOpiskelijat();
                        break;
                    }
                    case "T":
                    {
                        TestiopiskelijoidenLisäys();
                        break;
                    }
                }
            } while (input != "P");
        }

        public string LisääOpiskelija()
        {
            string asioid = "";
            while (true)
            {
                Console.Write("Anna AsioID: ");
                string t = Console.ReadLine();
                if (opiskelijat.Exists(x => x.AsioID == t) == true)
                {
                    Console.WriteLine("Tuo AsioID on jo käytössä!");
                }
                else if (String.IsNullOrEmpty(t) == true)
                {
                    Console.WriteLine("Kirjoita jotain!");
                }
                else
                {
                    asioid = t;
                    break;
                }
            }

            Console.Write("Anna etunimi: ");
            string etunimi = Console.ReadLine();
            
            Console.Write("Anna sukunimi: ");
            string sukunimi = Console.ReadLine();

            Console.Write("Anna ryhmä: ");
            string ryhmä = Console.ReadLine();
            
            Opiskelija opiskelija = new Opiskelija(etunimi, sukunimi, asioid, ryhmä);
            opiskelijat.Add(opiskelija);
            return "Opiskelija lisätty!"; 
        }
        public void NäytäOpiskelijat()
        {
            Console.WriteLine("\nEnsimmäinen opiskelija:\n" + opiskelijat.FirstOrDefault());
            Console.WriteLine("\nViimeinen opiskelija:\n" + opiskelijat.LastOrDefault());


            Console.WriteLine("\nKaikki opiskelijat:");
            foreach (Opiskelija opiskelija in opiskelijat)
            {
                Console.WriteLine(opiskelija.ToString());
            }

            opiskelijat.Sort((x, y) => x.Sukunimi.CompareTo(y.Sukunimi));
            Console.WriteLine("\nKaikki opiskelijat sukunimen mukaan:");

            foreach (Opiskelija opiskelija in opiskelijat)
            {
                Console.WriteLine(opiskelija.ToString());
            }
        }

        public void Valikko()
        {
            Console.WriteLine("\n(L)isää opiskelija");
            Console.WriteLine("(N)äytä opiskelijat");
            Console.WriteLine("(P)oistu");
            Console.WriteLine("(T)estiopiskelijoiden lisäys");
        }

        public string TestiopiskelijoidenLisäys()
        {
            opiskelijat.Add(new Opiskelija("Hanna", "Husso", "HH001", "TTV19S1"));
            opiskelijat.Add(new Opiskelija("Kirsi", "Kernel", "KK002", "TTV19S2"));
            opiskelijat.Add(new Opiskelija("Masa", "Niemi", "MN003", "TTV19S3"));
            opiskelijat.Add(new Opiskelija("Teppo", "Testaaja", "TT004", "TTV19SM"));
            opiskelijat.Add(new Opiskelija("Allan", "Aalto", "AA005", "TTV19SMM"));
            return "Opiskelijat lisätty!"; 
        }
    }
}