using System;

namespace T31MiniASIO
{
    public class Opiskelija
    {
        public string Etunimi { get; set; }
        public string Sukunimi { get; set; }
        public string AsioID { get; set; }
        public string Ryhmä { get; set; }

        public Opiskelija(string etunimi, string sukunimi, string asioid, string ryhmä)
        {
            Etunimi = etunimi;
            Sukunimi = sukunimi;
            AsioID = asioid;
            Ryhmä = ryhmä;
        }

        public override string ToString()
        {
            return $"{Etunimi}, {Sukunimi}, {AsioID}, {Ryhmä}";
        }
    }
}