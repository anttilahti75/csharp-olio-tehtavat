using System;
using System.Collections.Generic;
using System.Linq;

namespace T32Random
{
    class Person
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }

        public Person() { }
        public Person(string firstname, string lastname)
        {
            Firstname = firstname;
            Lastname = lastname;
        }

        public override string ToString()
        {
            return $"{Firstname} {Lastname}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {

            string list = TestListCollection(10000, 4, 10, 1000);
            string dict = TestDictionaryCollection(10000, 4, 10, 1000);

            Console.WriteLine(list);
            Console.WriteLine(dict);
        }

        static string TestListCollection(int personsCount, int firstnameLength, int lastnameLength, int searchPersonCount)
        {
            // Initialize variables.
            List<Person> persons = new List<Person>();
            long elapsedMs = 0;
            long totalTime = 0;
            string result = "";

            result += $"\nList collection\n##";
            
            // Setup and start a watch before measured method.
            var watch = System.Diagnostics.Stopwatch.StartNew();

            // Console.WriteLine("Adding persons to list.");
            for (var i = 0; i < personsCount; i++)
            {
                Person person = new Person(RandomName(firstnameLength), RandomName(lastnameLength));
                persons.Add(person);
            }

            // Stop watch after method has finished.
            watch.Stop();
            elapsedMs = watch.ElapsedMilliseconds;
            totalTime += elapsedMs;
            result += $"\nAdding time: {elapsedMs}";

            persons.Sort((x, y) => x.Firstname.CompareTo(y.Firstname));
            int foundPersons = 0;

            // Setup and start a watch before measured method
            watch.Restart();
            for (var i = 0; i < searchPersonCount; i++)
            {

                // randomize firstname // Pasi Mannisen korjausehdotus
                string firstname = RandomName(firstnameLength);

                foreach(Person p in persons) 
                {
                    if (firstname.Equals(p.Firstname)) { foundPersons++; break; }
                }
            }

            // Stop watch after method has finished.
            watch.Stop();
            elapsedMs = watch.ElapsedMilliseconds;
            totalTime += elapsedMs;

            result += $"\nPersons count: {persons.Count}\nPersons tried to find: {searchPersonCount}\nPersons found: {foundPersons}\nSearching: {elapsedMs}\nTotal: {totalTime}";
            return result;
        }

        static string TestDictionaryCollection(int personsCount, int firstnameLength, int lastnameLength, int searchPersonCount)
        {
            Dictionary<string, Person> persons = new Dictionary<string, Person>();
            long elapsedMs = 0;
            long totalTime = 0;
            string result = "";
            int duplicateKeys = 0;

            result += $"\nDictionary collection\n##";
            
            // Setup and start a watch before measured method
            var watch = System.Diagnostics.Stopwatch.StartNew();

            Console.WriteLine("Adding persons to list.");
            for (var i = 0; i < personsCount; i++)
            {
                var tempFirstname = RandomName(firstnameLength);
                if (persons.ContainsKey(tempFirstname) == true)
                {
                    duplicateKeys++;
                    i--;
                }
                else
                {
                    var tempLastname = RandomName(lastnameLength);
                    Person person = new Person(tempFirstname, tempLastname);
                    persons.Add(person.Firstname, person);
                }
            }

            // Stop watch after method has finished
            watch.Stop();
            elapsedMs = watch.ElapsedMilliseconds;
            totalTime += elapsedMs;
            result += $"\nAdding time: {elapsedMs}";

            // Sort keys to a separate list
            var list = persons.Keys.ToList();
            list.Sort();
            int foundPersons = 0;

            // Setup and start a watch before measured method
            watch.Restart();
            for (var i = 0; i < searchPersonCount; i++)
            {
                // Pick a random name
                string firstname = RandomName(firstnameLength);
                

                // Tuomo Hutun korjausehdotus
                if (persons.ContainsKey(firstname))
                {
                    foundPersons++;
                }
            }

            // Stop watch after method has finished
            watch.Stop();
            elapsedMs = watch.ElapsedMilliseconds;
            totalTime += elapsedMs;
            
            result += $"\nPersons count: {persons.Count}\nPersons tried to find: {searchPersonCount}\nPersons found: {foundPersons}\nSearching: {elapsedMs}\nTotal: {totalTime}\nDuplicate keys encountered: {duplicateKeys}";

            return result;
        }
        static string RandomName(int length)
        {
            // Define allowed characters
            char[] characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

            // Initialize random generator and target string
            Random rand = new Random();
            string str = "";

            // Fetch a random character and append it str
            for (var i = 0; i < length; i++)
            {
                str += characters[rand.Next(0, characters.Length)];
            }
            return str;
        }
    }
}