using System;

namespace T33Delegaatti
{
    public class Randomizer
    {
        public void MessUp(string value)
        {
            Random rand = new Random();
            char[] t = value.ToCharArray();
            for (int i = 0; i < value.Length; i++)
            {
                int j = rand.Next(i, value.Length);
                char temp = t[i];
                t[i] = t[j];
                t[j] = temp;
            }
            Console.WriteLine(t);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            do 
            {
                Randomizer randomizer = new Randomizer();
                string merkkiJono = "";
                
                Console.Write("Anna käsiteltävä merkkijono: ");
                string input = Console.ReadLine();
                merkkiJono = input;

                Console.WriteLine("Valitse haluamasi käsittely, voit antaa useamman käsittelyn kerralla");
                Console.WriteLine("yhtenä merkkijonona esim '123'");
                Console.WriteLine("1: isoiksi kirjaimiksi");
                Console.WriteLine("2: pieniksi kirjaimiksi");
                Console.WriteLine("3: palindromiksi");
                Console.WriteLine("4: sekoitetuksi");
                Console.WriteLine("0: lopetus");

                input = Console.ReadLine();

                if (input == "0")
                {
                    break;
                }

                StringHandler handler = default(StringHandler);
                for (int i = 0; i < input.Length; i++)
                {
                    switch(input[i])
                    {
                        case '1':
                        {
                            handler += CustomToUppercase;
                            break;
                        }
                        case '2':
                        {
                            handler += CustomToLowercase;
                            break;
                        }
                        case '3':
                        {
                            handler += CustomToPalindrome;
                            break;
                        }
                        case '4':
                        {
                            handler += randomizer.MessUp;
                            break;
                        }
                    }
                }
                handler(merkkiJono);
            } while (true);
        }

        public delegate void StringHandler(string value);

        public static void CustomToLowercase(string value)
        {
            Console.WriteLine(value.ToLower());
        }

        public static void CustomToUppercase(string value)
        {
            Console.WriteLine(value.ToUpper());
        }

        public static void CustomToPalindrome(string value)
        {
            char[] reversedValue = value.ToCharArray();
            Array.Reverse(reversedValue);
            Console.WriteLine(reversedValue);
        }
    }
}