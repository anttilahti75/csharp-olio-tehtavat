using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.IO;
using System.Linq;

namespace T34Lambda
{
    public class Friend
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public override string ToString()
        {
            return $"{Name} {Email}";
        }
    }

    public class MailBook
    {
        public List<Friend> FriendsList { get; } = new List<Friend>();
        
        string fileName = "tutut.csv";

        public MailBook()
        {
            try
            {
                string[] lines = System.IO.File.ReadAllLines(@fileName);
                foreach (string line in lines)
                {
                    var values = line.Split(';');
                    Friend friend = new Friend(){Name = values[0], Email = values[1]};
                    FriendsList.Add(friend);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public List<Friend> ShowFriends()
        {
            return FriendsList;
        }

        public void AddFriend(string name, string email)
        {
            Friend friend = new Friend(){Name = name, Email = email};
            FriendsList.Add(friend);
        }

        public bool SaveFriendList()
        {
            try
            {
                // Temporary lists are used because originals cannot
                // be modified while being used.
                List<Friend> tempList = new List<Friend>();
                List<Friend> toFileList = new List<Friend>();
                string[] lines = System.IO.File.ReadAllLines(@fileName);
                foreach (string line in lines)
                {
                    var values = line.Split(';');
                    Friend friend = new Friend(){Name = values[0], Email = values[1]};
                    // Add friends from the file to the list.
                    tempList.Add(friend);
                }
                // Check for friends who are not in the file yet.
                foreach (Friend friend in FriendsList)
                {
                    if (tempList.Exists(x => x.Name == friend.Name) != true)
                    {
                        toFileList.Add(friend); 
                    }
                }
                using (StreamWriter sw = File.AppendText(fileName)) 
                {
                    foreach (Friend friend in toFileList)
                    {
                        sw.WriteLine(friend.Name + ";" + friend.Email);
                    }
                }
                return true;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            MailBook mailbook = new MailBook();

            List<Friend> FriendsList = new List<Friend>();

            FriendsList = mailbook.ShowFriends();
            Console.WriteLine("Osoitekirjassa on {0} nimeä:", FriendsList.Count);
            foreach (Friend friend in FriendsList)
            {
                Console.WriteLine(friend.Name);
            }

            Console.Write("\nAnna haettavan tutun nimi tai sen osa: ");
            string tuttu = Console.ReadLine();

            var f = FriendsList.FindAll(x => x.Name.Contains(tuttu));

            if (f != null)
            {
                foreach (Friend friend in f)
                {
                    Console.WriteLine(friend);
                }
            }

            Console.WriteLine("Lisätään uusi ystävä:");
            Console.Write("Anna nimi: ");
            string nimi = Console.ReadLine();

            Console.Write("Anna email: ");
            string email = Console.ReadLine();

            mailbook.AddFriend(nimi, email);
            Console.WriteLine("Tallennetaan tiedostoon.");
            if (mailbook.SaveFriendList() == true)
            {
                Console.WriteLine("Tallennus onnistui.");
            }
            else { Console.WriteLine("Tallennus epäonnistui."); }
        }
    }
}