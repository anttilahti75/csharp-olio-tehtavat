using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace T35_Laskutoimituksia.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [DataTestMethod]
        [DataRow(new double[] { 2.0, 3.0 })]
        [DataRow(new double[] { })]
        public void SumTest(double[] arr)
        {
            double expected = 5.0;

            double actual = ArrayCalcs.Sum(arr);

            Assert.AreEqual(expected, actual);
        }

        [DataTestMethod]
        [DataRow(new double[] { 2.0, 3.0 })]
        [DataRow(new double[] { })]
        public void AverageTest(double[] arr)
        {
            double expected = 2.5;

            double actual = ArrayCalcs.Average(arr);

            Assert.AreEqual(expected, actual);
        }

        [DataTestMethod]
        [DataRow(new double[] { 2.0, 3.0 })]
        [DataRow(new double[] { })]
        public void MinTest(double[] arr)
        {
            double expected = 2.0;

            double actual = ArrayCalcs.Min(arr);

            Assert.AreEqual(expected, actual);
        }

        [DataTestMethod]
        [DataRow(new double[] { 2.0, 3.0 })]
        [DataRow(new double[] { })]
        public void MaxTest(double[] arr)
        {
            double expected = 3.0;

            double actual = ArrayCalcs.Max(arr);

            Assert.AreEqual(expected, actual);
        }
    }
}
