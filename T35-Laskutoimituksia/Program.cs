﻿using System;

namespace T35_Laskutoimituksia
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 1.0, 2.0, 3.3, 5.5, 6.3, -4.5, 12.0 };

            Console.WriteLine($"The array is:");
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write($"{array[i]} | ");
            }
            Console.WriteLine($"\nThe sum is: {ArrayCalcs.Sum(array)}");
            Console.WriteLine($"The average is: {ArrayCalcs.Average(array):F3}");
            Console.WriteLine($"The lowest is: {ArrayCalcs.Min(array)}");
            Console.WriteLine($"The highest is: {ArrayCalcs.Max(array)}");
        }
    }

    public class ArrayCalcs
    {
        public static double Sum(double[] arr)
        {
            double t = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                t += arr[i];
            }
            return t;
        }

        public static double Average(double[] arr)
        {
            double t = 0;
            for (var i = 0; i < arr.Length; i++)
            {
                t += arr[i];
            }
            return t = t / arr.Length;
        }
    
        public static double Min(double[] arr)
        {
            double t = 0;
            for (var i = 0; i < arr.Length; i++)
            {
                if(i != 0)
                {
                    if (arr[i] < t)
                    {
                        t = arr[i];
                    }
                }
                else
                { t = arr[i]; }
            }
            return t;
        }
    
        public static double Max(double[] arr)
        {
            double t = 0;
            for (var i = 0; i < arr.Length; i++)
            {
                if(i != 0)
                {
                    if (arr[i] > t)
                    {
                        t = arr[i];
                    }
                }
                else
                { t = arr[i]; }
            }
            return t;
        }
    }
}