using System;
using System.Collections.Generic;

namespace T37_Ostokset
{
    class Product
    {
        public string Name { get; }
        public double Price { get; }
        public int Quantity { get; }

        public double TotalPrice()
        {
            return Price * Quantity;
        }

        public override string ToString()
        {
            return $"{Name} {Price} {Quantity}";
        }

        public Product(string name, double price, int quantity)
        {
            Name = name;
            Price = price;
            Quantity = quantity;
        }
    }
    class Invoice
    {
        public List<Product> invoiceItems { get; } = new List<Product>();
        public string Customer { get; }
        
        public void AddProduct(Product product)
        {
            invoiceItems.Add(product);
        }
        public int GetTotalAmount()
        {
            int t = 0;
            invoiceItems.ForEach(x => t += x.Quantity);
            return t;
        }
        public double GetTotalPrice()
        {
            double t = 0;
            foreach (Product o in invoiceItems)
            {
                t += o.TotalPrice();
            }
            return t;
        }
        
        public Invoice(string cust)
        {
            Customer = cust;
        }
    }
    class CashRegister
    {
        List<Invoice> invoices { get; } = new List<Invoice>();
        public void AddInvoice(Invoice invoice)
        {
            invoices.Add(invoice);
        }
        public string ShowInvoice(Invoice invoice)
        {
            string str = "";
            str += $"Invoice for customer: {invoice.Customer}\n";
            str += "===\n";
            foreach (Product item in invoice.invoiceItems)
            {
                str += $"{item.Name}\t{item.Price}\t{item.Quantity} pieces\t{item.TotalPrice()}e total\n";
            }
            str += "===\n";
            str += $"Total: {invoice.GetTotalAmount()} products {invoice.GetTotalPrice()} euros";
            return str;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            CashRegister reg1 = new CashRegister();
            
            Invoice invoice01 = new Invoice("Kirsi Kernel");
            invoice01.AddProduct(new Product("Orange", 0.33, 32));
            invoice01.AddProduct(new Product("Apples", 0.22, 40));
            reg1.AddInvoice(invoice01);
            Console.WriteLine($"\n{reg1.ShowInvoice(invoice01)}");

            Invoice invoice02 = new Invoice("Matti Mallikas");
            invoice02.AddProduct(new Product("Tea", 0.33, 32));
            invoice02.AddProduct(new Product("Biscuits", 0.22, 40));
            reg1.AddInvoice(invoice02);
            Console.WriteLine($"\n{reg1.ShowInvoice(invoice02)}");

            // Without CashRegister use the following style.
            //
            // Console.WriteLine($"Invoice for customer: {invoice01.Customer}");
            // Console.WriteLine("===");
            // invoice01.invoiceItems.ForEach(x => Console.WriteLine($"{x.Name}\t{x.Price}e\t{x.Quantity} pieces\t{x.TotalPrice()}e total"));
            // Console.WriteLine("===");
            // invoice01.invoiceItems.ForEach(x => Console.WriteLine($"Total: "));
            // Console.WriteLine($"Total: {invoice01.GetTotalAmount()} products {invoice01.GetTotalPrice()} euros");
        }
    }
}