using System;

namespace T38_Noppa
{
    class Program
    {
        static void Main(string[] args)
        {
            Dice dice = new Dice(6);
            Console.WriteLine($"Dice, one test throw value is {dice.Throw()}");

            Console.Write("How many times do you want to throw a dice: ");
            int input = Int32.Parse(Console.ReadLine());

            int[] results = new int[input];

            for (int i = 0; i < input; i++)
            {
                results[i] = dice.Throw();
            }
            
            Console.WriteLine($"Dice has been thrown {input} times, average is {Average(results)}");
            
            for (int i = 1; i <= dice.Faces; i++)
            {
                Console.WriteLine($"Average for {i} is {Array.FindAll(results, x => x == i).Length}");
            }

            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }

        public static double Average(int[] arr)
        {
            double t = 0;
            for (var i = 0; i < arr.Length; i++)
            {
                t += arr[i];
            }
            return t = t / arr.Length;
        }
    }

    class Dice
    {
        public int Faces { get; }

        public Dice(int faces) { 
            Faces = faces;
        }
        public int Throw()
        {
            Random rand = new Random();

            // The maximum is exclusive so add one to get the right number of faces.
            return rand.Next(1, Faces + 1);
        }
    }
}