using System;

namespace T39_Kalat
{
    public class Kala
    {
        public string Laji { get; set; }
        public int Pituus { get; set; }
        public double Paino { get; set; }

        public Kalastaja Nappaaja { get; set; }

        public void setNappaaja(Kalastaja k)
        {
            Nappaaja = k;
        }

        public override string ToString()
        {
            return $"Laji: {Laji}, pituus: {Pituus} cm, paino: {Paino:F2} kg";
        }

        // Destructor announces when fish-object is removed.
        // Just for learning object lifecycle.
        ~Kala()
        {
            Console.WriteLine("Kala-olio poistettu");
        }
    }
}