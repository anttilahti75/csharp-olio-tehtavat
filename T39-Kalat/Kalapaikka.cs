using System;
using System.Collections.Generic;

namespace T39_Kalat
{
   public class Kalapaikka
    {
        public string Nimi { get; set; }
        public string Paikka { get; set; }
        // Initialize an array of available fish species.
        public string[] KalaLajit = {"Lohi", "Sampi", "Siika", "Särki", "Hauki"};
        public List<Kala> Kalat { get; } = new List<Kala>();

        // Constructor which also gets a random amount of fishes.
        // Fish properties are also randomized.
        public Kalapaikka()
        {
            for (int i = 0; i < MuutaKalaista("määrä"); i++)
            {
                Kalat.Add(new Kala(){ Laji = KalanLajittelija(), Pituus = MuutaKalaista("pituus"), Paino = KalanPainottaja()});
            }
        }

        // Randomize fish weight.
        public double KalanPainottaja()
        {
            Random rand = new Random();
            return (rand.NextDouble() * 3);
        }

        // Random fish species for the lake.
        public string KalanLajittelija()
        {
            Random rand = new Random();
            return KalaLajit[rand.Next(0,KalaLajit.Length)];
        }

        // Random length for the fish or the amount of fishes for the lake.
        // Depends on the argument given.
        public int MuutaKalaista(string ominaisuus)
        {
            Random rand = new Random();
            if (ominaisuus == "pituus") { return rand.Next(10, 40); }
            else if (ominaisuus == "määrä" ) { return rand.Next(4, 20); }
            else return 1;
        }

        public override string ToString()
        {
            return $"Kalapaikka: {Nimi}, sijainti: {Paikka}";
        }
    }
}