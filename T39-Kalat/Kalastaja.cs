using System.Collections.Generic;

namespace T39_Kalat
{
    public class Kalastaja
    {
        public string Nimi { get; set; }
        public string PuhNro { get; set; }
        public List<Kala> Kalasaalis { get; } = new List<Kala>();

        // Add fish-object to catches.
        public void NappaaKala(Kala kala)
        {
            Kalasaalis.Add(kala);
        }

        public override string ToString()
        {
            return $"Kalastaja: {Nimi}, puh.nro: {PuhNro}";
        }
    }
}