using System;
using System.Collections.Generic;
using System.Linq;

namespace T39_Kalat
{
    public class MyFishApp
    {
        public void Begin()
        {
            // Initialize lists for fishers and one static fishing location.
            List<Kalastaja> kalastajat = new List<Kalastaja>();
            Kalapaikka apaja1 = new Kalapaikka(){ Nimi = "Kalajärvi", Paikka = "Järvikylä" };
            string input = "";

            do
            {
                Valikko();
                Console.Write("Anna valinta: ");
                input = Console.ReadLine();

                switch (input)
                {
                    // Add fisherman.
                    case "R":
                    {
                        kalastajat.Add(RekisteröiKalastaja());
                        Console.WriteLine($"Uusi kalastaja rekisteröity!");
                        Console.WriteLine($"Rekisterissä on nyt {kalastajat.Count} kalastajaa");
                        break;
                    }
                    // Choose a fisher and get to the fisher actions.
                    case "K":
                    {
                        if (kalastajat.Count > 0)
                        {
                            kalastajat.ForEach(x => Console.WriteLine(x.Nimi));
                            Console.Write("Ketä käyt moikkaamassa? ");
                            input = Console.ReadLine();
                            if (kalastajat.Exists(x => x.Nimi == input) == true)
                            {
                                KalastajaValikko(kalastajat.Find(x => x.Nimi == input), apaja1);
                            } else { Console.WriteLine("Ei löydy!"); }
                        }
                        else
                        {
                            Console.WriteLine("Järvellä on hiljaista...");
                        }
                        break;
                    }
                    // Show the catch collection of every fisher.
                    case "N":
                    {
                        if (kalastajat.Count > 0)
                        {
                            kalastajat.ForEach(x => {
                                Console.WriteLine($"\n{x.ToString()}");
                                x.Kalasaalis.ForEach(
                                    y => Console.WriteLine(y.ToString())
                                    );
                            });
                        }
                        else
                        {
                            Console.WriteLine("Täällä ei ole yhtään kalastajaa.");
                        }

                        break;
                    }
                    // Sort all catches by their weight in descending order.
                    case "L":
                    {
                        Console.WriteLine("Noniin, katsotaanpas mikä kala on suurin");
                        // This sorts all fishes by their weight with additional info.
                        // SelectMany hinted by Pasi Manninen
                        kalastajat.SelectMany(k => k.Kalasaalis).OrderByDescending(
                            f => f.Paino).ToList().ForEach(
                                o => {
                                    Console.WriteLine($"\n- {o.ToString()}");
                                    Console.WriteLine($"- Apaja: {apaja1.Nimi}");
                                    Console.WriteLine($"- Paikka: {apaja1.Paikka}");
                                    Console.WriteLine($"- {o.Nappaaja.ToString()}");
                                    }
                                );
                        break;
                    }
                }
            } while (input != "P");

        }

        public Kalastaja RekisteröiKalastaja()
        {
            Console.Write("Anna nimi: ");
            string nimi = Console.ReadLine();
            
            Console.Write("Anna puhelinnumero: ");
            string puhNro = Console.ReadLine();
            
            Kalastaja k = new Kalastaja() { Nimi = nimi, PuhNro = puhNro };
            return k;
        }
        public void KalastajaValikko(Kalastaja k, Kalapaikka p)
        {
            // Catch a fish.
            Console.WriteLine("\n(N)appaa kala!");
            // Get back to main menu.
            Console.WriteLine("(T)akaisin");

            string input = "";

            do
            {
                Console.Write("Anna valinta: ");
                input = Console.ReadLine();

                switch (input)
                {
                    case "N":
                    {
                        if (p.Kalat.Count != 0)
                        {
                            // Here we decide randomly what kind of fish the fisher catches.
                            // Initialize a temporary array for fishes.
                            Kala[] tempKalat = new Kala[p.Kalat.Count];
                            // Assign fishes of the fishing location to the temporary array.
                            tempKalat = p.Kalat.ToArray();
                            // Initialize randomizer.
                            Random rand = new Random();
                            int i = rand.Next(0, tempKalat.Length);
                            // Pick a random fish by species.
                            Kala kala = p.Kalat.Find(x => x.Laji == tempKalat[i].Laji);
                            // Fisher action.
                            k.NappaaKala(kala);
                            // Kala action, who catched this fish?.
                            kala.Nappaaja = k;
                            // Remove the fish from the lake so there aren't duplicates and the lake would actually go empty.
                            p.Kalat.Remove(kala);
                            Console.WriteLine($"{k} nappasi kalan!");
                            Console.WriteLine($"Kalansaalis:");
                            k.Kalasaalis.ForEach(x => Console.WriteLine(x.ToString()));
                        }
                        else { Console.WriteLine("Täällä ei taida olla yhtään kalaa."); }
                        
                        break;
                    }
                }
            } while (input != "T");
        }

        public void Valikko()
        {
            Console.WriteLine("\n(R)ekisteröi kalastaja");
            Console.WriteLine("(K)äy moikkaamassa kalastajaa");
            Console.WriteLine("(N)äytä kalastajien kalansaaliit");
            Console.WriteLine("(L)aita kalat järjestykseen");
            Console.WriteLine("(P)oistu");
        }
    }
}