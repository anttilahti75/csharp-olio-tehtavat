using System;

namespace T4PalindromiApplication
{
    class Palindromi
    {
        static void Main(string[] args)
        {
            // Declare variable(s)
            string userInput, originalSentence;
            bool isPalindrome = true;

            // Ask user input
            Console.Write("Give a sentence: ");
            userInput = Console.ReadLine();

            // Remove spaces from the input and split it as separate words
            string[] words = userInput.Split(' ', System.StringSplitOptions.RemoveEmptyEntries);

            // Join the words in to a single string
            originalSentence = string.Join("", words);

            // Prepare two arrays for comparison
            char[] original = originalSentence.ToCharArray();
            char[] originalReverse = originalSentence.ToCharArray();

            // Reverse one of the arrays
            Array.Reverse( originalReverse );

            /* 
            Compare individual characters of the sentences.
            Since palindromes are the same from left to right and backwards,
            we can compare normal and reversed array by the same positions.
            If same positions have different letter, the sentence is not a palindrome.
            NOTE: commas etc. screw this up, only spaces are removed earlier.
            */
            for (int a = 0; a < originalSentence.Length; a = a + 1)
            {
                if (original[a] != originalReverse[a])
                {
                    isPalindrome = false;
                }
            }

            // Display results
            if (isPalindrome == true)
            {
                Console.WriteLine("The string is a palindrome");
            }
            else if (isPalindrome == false)
            {
                Console.WriteLine("The string is not a palindrome");
            }
            Console.ReadLine();
        }
    }
}