﻿using System;
using System.Linq;

namespace T40_Kuviot
{
    class Program
    {
        static void Main(string[] args)
        {
            Shapes shapeHolder = new Shapes();

            shapeHolder.shapesList.Add(new Circle(1.0));
            shapeHolder.shapesList.Add(new Circle(2.0));
            shapeHolder.shapesList.Add(new Circle(3.0));
            shapeHolder.shapesList.Add(new Rectangle(10.0, 20.0));
            shapeHolder.shapesList.Add(new Rectangle(20.0, 30.0));
            shapeHolder.shapesList.Add(new Rectangle(30.0, 40.0));

            shapeHolder.shapesList.OfType<Circle>().ToList().ForEach(
                x => Console.WriteLine(
                    $"Circle: radius {x.Radius:F2}, area {x.Area():F2}, circumference {x.Circumference():F2}"));
            
            shapeHolder.shapesList.OfType<Rectangle>().ToList().ForEach(
                x => Console.WriteLine(
                    $"Rectangle: width {x.Width:F2}, height {x.Height:F2}, area {x.Area():F2}, circumference {x.Circumference():F2}"));
        }
    }
}
