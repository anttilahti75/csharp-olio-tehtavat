using System;
using System.Collections.Generic;

namespace T40_Kuviot
{
    public abstract class Shape
    {
        public string Nimi { get; set; }
        public abstract double Area();
        public abstract double Circumference();
    }

    public class Shapes
    {
        public List<Shape> shapesList { get; } = new List<Shape>();
    }

    public class Circle : Shape
    {
        public double Radius { get; }
        public override double Area()
        {
            double pi = Math.PI;
            return (pi * Radius * Radius);
        }

        public override double Circumference()
        {
            double pi = Math.PI;
            return (pi * Radius * 2);
        }

        public Circle(double radius)
        {
            Radius = radius;
        }
    }

    public class Rectangle : Shape
    {
        public double Width { get; }
        public double Height { get; }
        public override double Area()
        {
            return Width * Height;
        }

        public override double Circumference()
        {
            return (Width * 2 + Height * 2);
        }

        public Rectangle(double width, double height)
        {
            Width = width;
            Height = height;
        }
    }
}