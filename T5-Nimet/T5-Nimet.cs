using System;
using System.Collections.Generic;

namespace T5Nimet
{
   class Nimet
   {
        static void Main(string[] args)
        {
            // Declare variables
            List<string> nimet = new List<string>();
            string userInput;

            do
            {
                // Ask user input
                Console.Write("Give a name: ");
                userInput = Console.ReadLine();

                // If user enters something, continue asking, otherwise stop the loop
                if (userInput != "")
                {
                    nimet.Add(userInput);
                }
                else
                {
                    break;
                }
            }
            while (true);

            // Default alphabetical sort
            nimet.Sort();

            // Display array
            foreach (string v in nimet)
            {
                Console.WriteLine(v);
            }
            Console.ReadLine();
        }
    }
}