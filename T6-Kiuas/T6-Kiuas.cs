using System;

namespace T6KiuasApplication
{
    class Kiuas
    {
        public bool Power { get; set; }
        public double Temperature { get; set; }
        public double Humidity { get; set; }

        public void PrintData()
        {
            Console.WriteLine("Is power on? " + Power);
            Console.WriteLine("Temperature is " + Temperature + " degrees celsius");
            Console.WriteLine("Humidity is " + Humidity + "%");
        }
    }

    class Program
    {
        // The main program
        static void Main(string[] args)
        {
            Kiuas harvia = new Kiuas();
            harvia.PrintData();
            harvia.Power = true;
            harvia.Temperature = 120.0;
            harvia.Humidity = 60;
            harvia.PrintData();

            Console.ReadLine();
        }
    }
}