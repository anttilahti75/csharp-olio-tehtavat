using System;

namespace T7PesukoneApplication
{
    class Pesukone
    {
        public string Model { get; set; }
        public bool Power { get; set; }
        public bool Water { get; set; }
        public int Temperature { get; set; }
        public int WashingTime { get; set; }

        public Pesukone()
        {
            Power = true;
        } 

        public Pesukone(string argModel)
        {
            Model = argModel;
        }

        public Pesukone(string argModel, bool argPower, bool argWater)
        {
            Model = argModel;
            Power = argPower;
            Water = argWater;
        }
    }

    class Program
    {
        // The main program
        static void Main(string[] args)
        {

            Pesukone whirlpool = new Pesukone();
            whirlpool.Model = "Whirlpool";
            
            Pesukone electrolux = new Pesukone("Electrolux");
            electrolux.Power = true;
            
            Pesukone lg = new Pesukone("LG", true, true);
            
            Pesukone philips = new Pesukone
            { 
                Model = "Philips", 
                Power = true, 
                Water = true,
                Temperature = 60,
                WashingTime = 40
            };

            Console.WriteLine(whirlpool.Model);
            
            Console.WriteLine("model is {0} and power is {1}",
                electrolux.Model,
                electrolux.Power
            );
            
            Console.WriteLine("model is {0}, power is {1}, water is {2}",
                lg.Model,
                lg.Power,
                lg.Water
            );
            
            Console.WriteLine("model is {0}, power is {1}, water is {2}, temperature is {3} degrees celsius, washing time is {4} minutes", 
                philips.Model, 
                philips.Power,
                philips.Water,
                philips.Temperature,
                philips.WashingTime
            );

            Console.ReadLine();
        }
    }
}