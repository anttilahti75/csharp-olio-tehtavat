using System;

namespace T8TelevisioApplication
{
    class Program
    {
        // The main program
        static void Main(string[] args)
        {
            
            Televisio lg = new Televisio("LG");

            Console.WriteLine("Toggling power: " + lg.TogglePower());
            Console.WriteLine("Setting up channel list: " + lg.ReadyChannels());
            Console.WriteLine("Changing channel: " + lg.SetChannel(-2));
            
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Turning volume up: " + lg.VolUp());
            }
            
            Console.WriteLine("Getting TV info: " + lg.ToString());
            Console.WriteLine("Toggling power: " + lg.TogglePower());

            Console.ReadLine();
        }
    }
}