using System;

namespace T8TelevisioApplication
{
    class Televisio
    {
        private string _model;
        private bool _power;
        private int _volume;
        private int _channel;
        private bool _channelsReady;

        public string TogglePower()
        {
            if ( _power == false )
            {
                _power = true; return "Power is on";
            } 
            else
            {
                _power = false; return "Power is off";
            }
        }

        public int VolUp()
        {
            if ( _volume < 100 )
            {
                _volume += 1; return _volume;
            }
            else
            {
                return _volume;
            }
        }

        public int VolDown()
        {
            if ( _volume > 0 )
            {
                _volume -= 1;
                return _volume;
            }
            else
            {
                return _volume;
            }
        }

        public int SetChannel(int value)
        {
            if ( value >= 1 && value <= 255 )
            {
                _channel = value;
                return _channel;
            }
            else
            {
                return _channel;
            }
        }

        public string ReadyChannels()
        {
            if ( _channelsReady == false )
            {
                _channelsReady = true;
                return "Channels are set";
            }
            else
            {
                return "Channels are already set";
            }
        }

        public override string ToString()
        {
            return $"\nTV:\nModel: {_model}\nPowered on: {_power}\nVolume: {_volume}\nChannel: {_channel}";
        }

        // Constructor
        public Televisio(string model)
        {
            _model = model;
            _power = false;
            _volume = 0;
            _channel = 1;
            _channelsReady = false;
        }
    }
}