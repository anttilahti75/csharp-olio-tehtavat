using System;

namespace T9VehicleApplication
{
    class Program
    {
        // The main program
        static void Main(string[] args)
        {
            
            Vehicle bicycle = new Vehicle
            {
                Brand = "Helkama",
                Model = "Jopo",
                Speed = 20,
                Tyres = 2
            };
            
            Console.WriteLine(bicycle.ShowInfo());
            Console.WriteLine(bicycle.ToString());
            bicycle.Model = "Aino";
            bicycle.Speed = 40;
            Console.WriteLine(bicycle.ShowInfo());
            Console.WriteLine(bicycle.ToString());

            Vehicle car = new Vehicle
            {
                Brand = "Skoda",
                Model = "Felicia",
                Speed = 80,
                Tyres = 4
            };

            Console.WriteLine(car.ShowInfo());
            Console.WriteLine(car.ToString());
            car.Model = "Octavia";
            car.Speed = 120;
            Console.WriteLine(car.ShowInfo());
            Console.WriteLine(car.ToString());

            Console.ReadLine();
        }
    }
}