using System;

namespace T9VehicleApplication
{
    class Vehicle
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public int Speed { get; set; }
        public int Tyres { get; set; }

        public string ShowInfo()
        {
            return $"brand: {Brand} model: {Model}";
        }

        public override string ToString()
        {
            return $"brand: {Brand}, model: {Model}, speed: {Speed}, tyres: {Tyres}";
        }
    }
}